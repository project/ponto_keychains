<?php

/**
 * This is a public interface for a
 * generic Cipher class.
 */
interface CipherInterface {
  function encrypt($input);
  function decrypt($input);
}

?>