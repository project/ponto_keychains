<?php

interface KeyChain {
  public function getID();
  public function getUsername();
  public function setUsername($name);
  public function getPassword();
  public function setPassword($password);
}

?>