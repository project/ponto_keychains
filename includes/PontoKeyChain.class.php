<?php

/**
 * @abstract
 * The KeyChain class holds information to connect to a resource. This is the
 * "key" to the resource. This "KeyChain" can hold a username, password,
 * ssh-key, public-key, etc.
 *
 */
class PontoKeyChain implements KeyChain {
  private $ssh_key = null;
  private $ssh_key_type = null;
  private $username = '';
  private $password = '';
  private $kcid = null;
  private $kc_info = null;
  private $create_new = false;
  private $autopilot = null;
  private $cipher = null;
  private $updatePassword = false;

  public function __construct($kcid = null) {
    $this->setCipherKey(variable_get('ponto_keychain_cipher_key', ''));
    if($kcid === null) {
      return;
    }
    else {
      $this->load($kcid);
    }

  }

  public function setType($type) {
    $this->kc_info->kc_type = $type;
  }
  public function getType() {
    return $this->kc_info->kc_type;
  }

  public function setCipherKey($cipher_key) {
    $this->cipher = new PontoCipher($cipher_key);
  }

  public static function getUserKeychains() {
    global $user;
    $rs = db_query("SELECT * FROM {ponto_keychain} WHERE uid = %d", $user->uid);
    $kc = array();
    while($row = db_fetch_object($rs)) {
      $kc[] = $row;
    }
    return $kc;
  }

  /**
   * Loads the key chain
   *
   * @params int $key_id The ID of the KeyChain to load
   *
   * @return void
   */
  private function load($kcid) {
    $this->kc_info = db_fetch_object(db_query("SELECT * FROM {ponto_keychain} WHERE kcid = %d", $kcid));
    $info = $this->kc_info;
    $this->username = $info->kc_username;
    $this->password = $info->kc_password;
    $this->kcid = $info->kcid;
    return;
  }

  public function createNew($kcid) {
    $this->kcid = $kcid;
    $this->kc_info = new stdClass();
    $this->create_new = true;
  }

  /**
   * Note: We can not user db_next_id. So let us not worry about
   * that. We will defer the ID setting to the developer.
   */
  public function save() {
    global $user;

    if($this->create_new) {
      // TODO: Check for collission
      $try = db_query("INSERT INTO {ponto_keychain}(kcid, uid) VALUES(%d, %d)", $this->getID(), $user->uid);
    }
    else if($this->kcid == null) {
      return false;
    }
    else {
      $try = true;
    }

    if($try == false) {
      return false;
    }

    #Validations

    # Update
    $try = db_query("UPDATE {ponto_keychain} SET
             kc_title = '%s', kc_description = '%s',
             kc_username = '%s',
             kc_type = %d
             WHERE kcid = %d",
             $this->getName(), $this->getDescription(), $this->getUsername(),
             $this->getType(),
             $this->getID());
    # Encrypt the password
    if($this->updatePassword) {
      $try = $try && db_query("UPDATE {ponto_keychain}
                            SET kc_password = '%s' WHERE kcid = %d",
                            $this->password,
                            $this->getID());
    }
    else if($this->password == null) {
      $try = $try && db_query("UPDATE {ponto_keychain}
                            SET kc_password = '%s' WHERE kcid = %d",
                            null,
                            $this->getID());
    }
    if($this->getSSHKeyPath() != '') {
      $try = $try && db_query("UPDATE {ponto_keychain}
                              SET kc_ssh_key_path = '%s'
                              WHERE kcid = %d",
                              $this->getSSHKeyPath(), $this->getID());
    }
    else if($this->getSSHKey() != '') {
      $try = $try && db_query("UPDATE {ponto_keychain}
                            SET kc_ssh_key = '%s' WHERE kcid = %d",
                            $this->cipher->encrypt($this->getSSHKey()),
                            $this->getID());
    }
    return $try;
  }

  public function getName() {
    return $this->kc_info->kc_title;
  }
  public function setName($name) {
    $this->kc_info->kc_title = $name;
  }
  public function getDescription() {
    return $this->kc_info->kc_description;
  }
  public function setDescription($desc) {
    $this->kc_info->kc_description = $desc;
  }

  public function getUsername() {
    return $this->username;
  }
  public function setUsername($user) {
    $this->username = $user;
  }
  public function getPassword() {
    if($this->password != '') {
      return $this->cipher->decrypt($this->password);
    }
  }
  public function setPassword($pass) {
    $this->updatePassword = true;
    $this->password = $this->cipher->encrypt($pass);
    //$this->password = $pass;
  }
  public function setSSHKey($key) {
    $this->kc_info->kc_ssh_key = $key;
  }
  public function getSSHKey() {
    return $this->kc_info->kc_ssh_key;
  }
  public function getSSHMD5() {
    if($this->kc_info->kc_ssh_key != '') {
      return md5($this->kc_info->kc_ssh_key);
    }
    else {
      return 'EMPTY';
    }
  }
  public function setSSHKeyPath($path) {
    $this->kc_info->kc_ssh_key_path = $path;
  }
  public function getSSHKeyPath() {
    return $this->kc_info->kc_ssh_key_path;
  }




  /**
   * Return the KeyChainID of this instance.
   *
   * @return int The KeyChainID of this instnace.
   */
  public function getID() {
    return $this->kcid;
  }
}

?>
