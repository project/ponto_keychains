<?php

/**
 *
 * Notes: Refrain from using db_nextid for upgrade
 * purposes.
 */
define('PONTO_KEYCHAIN_TYPE_GLOBAL', 1); // Anyone can use
define('PONTO_KEYCHAIN_TYPE_USER', 2); // Only the user can use
define('PONTO_KEYCHAIN_TYPE_SYSTEM', 3); // Use for modules, system, drupal, etc.

function ponto_keychain_perm() {
  return array(
    'ponto kc create',
    'ponto kc view',
    'ponto kc delete',
    'ponto kc admin',
  );
}

function ponto_keychain_node_info() {
  return array(
    'ponto_keychain' => array(
      'name' => t('Ponto Keychain'),
      'module' => 'ponto_keychain',
      'description' => t('A Keychain is a set of authentication services
                         such as a username, password, or SSL Certificate.'),
    ),
  );
}

function _ponto_keychain_get_user_types() {
  $types = array(
    PONTO_KEYCHAIN_TYPE_USER => 'USER',
    PONTO_KEYCHAIN_TYPE_GLOBAL => 'GLOBAL',
    PONTO_KEYCHAIN_TYPE_SYSTEM => 'SYSTEM',
  );
  return $types;
}
function ponto_keychain_form($node) {
  $form = array();
  $kc = new PontoKeyChain($node->nid);
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $node->title,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $node->description,
  );
  $form['kc_type'] = array(
    '#type' => 'select',
    '#title' => t('Key Chain Type'),
    '#description' => t('This is the visibility of this Key Chain to others.
      A %user type is visible ONLY to you and useable only to you. This is the most common type.
      A %global type is visible to ANYONE on the system. This type, and the permission to
      create this type is usually granted only to power users and administrators.
      The %system type is usable only by modules and system items.',
      array('%user' => 'USER', '%global' => 'GLOBAL', '%system' => 'SYSTEM')),
    '#options' => _ponto_keychain_get_user_types(),
    '#default_value' => $kc->getType(),
  );
  $form['kc_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => $kc->getUserName());
  $form['kc_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('Not shown for security reasons.
                        Enter a password to update the password, else leave blank.'),
  );
  $form['kc_password_confirmation'] = array(
    '#type' => 'password',
    '#title' => t('Confirm Password'),
    '#description' => t('Confirm the password that you entered.'),
  );
  $form['kc_empty_password'] = array(
    '#type' => 'checkbox',
    '#title' => t('Empty password'),
    '#description' => t('Check this box to have your password emptied and
                        set to null. This is good to do if you are
                        being authenticated by username only and the
                        IP address allows you to not use a password, thus
                        in effect no password will be sent.'),
  );
  $form['key_info'] = array('#value' => t('Your SSH key is not show for
    security reasons. However an MD5 checksum is provided to allow you
    to verify what SSH Key is currently being used; or you can re-enter
    the SSH key. The current MD5 checksum is: !md5_sum',
    array('!md5_sum' => '<strong>' . $kc->getSSHMD5() . '</strong>')));
  $form['kc_ssh_key'] = array(
    '#type' => 'textarea',
    '#title' => t('SSH Private Key'),
    '#description' => t('An OpenSSL Private Key certificate. Please note that
                        this is different than a PuTTYy certificate.
                        This is currenlty NOT used due to some security concerns.
                        It is recommended that you set the SSH Key Path if you wish
                        to use a private key for authentication.'),
    '#default_value' => $kc->getSSHKey(),
  );
  $form['kc_ssh_key_path'] = array(
    '#type' => 'textarea',
    '#title' => t('SSH Configuration Path'),
    '#description' => t('The path to the ssh key to use. Note: if you
                        enter in a path here, the ssh_key that you entered above
                        will be ignored.'),
    '#default_value' => $kc->getSSHKeyPath(),
  );

  return $form;
}

/**
 * Validates the node
 */
function ponto_keychain_validate($node) {
  if($node->kc_password != $node->kc_password_confirmation && $node->kc_password != "") {
    form_set_error('kc_password', t('Passwords do not match.'));
    form_set_error('kc_password_confirmation', '');
  }
  return;
}
/**
 * Saves/Updates a KeyChain
 * Implementation of hook_insert
 */
function ponto_keychain_insert($node) {
  // Save the keychain
  $kcid = $node->nid;
  $keychain = new PontoKeyChain($kcid);
  if($node->is_new) {
    $keychain->createNew($node->nid);
  }
  $keychain->setCipherKey(variable_get('ponto_keychain_cipher_key', null));
  $keychain->setName($node->title);
  $keychain->setDescription($node->description);
  $keychain->setType($node->kc_type);
  $keychain->setUsername($node->kc_username);
  if($node->kc_password != '') {
    $keychain->setPassword($node->kc_password);
  }
  if($node->kc_empty_password) {
    $keychain->setPassword(null);
  }
  if($node->kc_ssh_key) {
    $keychain->setSSHKey($node->kc_ssh_key);
  }
  if($node->kc_ssh_key_path) {
    $keychain->setSSHKeyPath($node->kc_ssh_key_path);
  }
  $keychain->save();
}

function ponto_keychain_update($node) {
  // Save the keychain
  $kcid = $node->nid;
  $keychain = new PontoKeyChain($kcid);
  $keychain->setCipherKey(variable_get('ponto_keychain_cipher_key', null));
  $keychain->setName($node->title);
  $keychain->setDescription($node->description);
  $keychain->setType($node->kc_type);
  $keychain->setUsername($node->kc_username);
  if($node->kc_password != '') {
    $keychain->setPassword($node->kc_password);
  }
  if($node->kc_empty_password) {
    $keychain->setPassword(null);
  }
  
  if($node->kc_ssh_key_path) {
    $keychain->setSSHKeyPath(str_replace("\n", "", $node->kc_ssh_key_path));
    error_log($node->kc_ssh_key_path);
  }
  if($node->kc_ssh_key) {
    $keychain->setSSHKey($node->kc_ssh_key);
  }
  $keychain->save();  
}

/**
 * Implementation of hook_load
 * We do not load anything here because
 * everything is encrypted, and should 
 * only be decrypted explicitly.
 */
function ponto_keycahin_load($node) {
  return;
}

function ponto_keychain_menu($may_cache) {
  _ponto_keychain_init();
  $items = array();
  
  if($may_cache) {
    
  }
  # Adminitration
  $items[] = array(
    'path' => 'admin/settings/ponto_keychain',
    'title' => t('Ponto Keychain'),
    'description' => t('Administration settings for the Ponto Keychain module.'),
    'callback' => 'ponto_keychain_admin_page',
    'access' => user_access('ponto kc admin'),
  );
  # User Tab
  $items[] = array(
    'path' => 'user/' . arg(1) . '/keychains',
    'title' => t('My Key Chains'),
    'description' => t('Key Chains that belong to the user'),
    'access' => user_access('ponto kc view') | user_access('ponto kc admin'),
    'type' => MENU_LOCAL_TASK,
    'callback' => 'ponto_keychain_cp',
    'callback arguments' => array(arg(1)),
  );
  # Personal
  $items[] = array(
    'path' => 'user/' . arg(1) . '/keychains/personal',
    'title' => t('Personal Key Chains'),
    'description' => t('Key Chains that belong to the user'),
    'access' => user_access('ponto kc view') | user_access('ponto kc admin'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'callback' => 'ponto_keychain_cp',
    'callback arguments' => array(arg(1), 'user'),
  );
  # Global
  $items[] = array(
    'path' => 'user/' . arg(1) . '/keychains/shared',
    'title' => t('Shared'),
    'description' => t('Key Chains that belong to the user'),
    'access' => user_access('ponto kc view') | user_access('ponto kc admin'),
    'type' => MENU_LOCAL_TASK,
    'callback' => 'ponto_keychain_cp',
    'callback arguments' => array(arg(1), 'global'),
  );
  return $items;
}

/**
 * Administration settings
 */
function ponto_keychain_admin_page() {
  return drupal_get_form('ponto_keychain_admin_form');
}
function ponto_keychain_admin_form() {
  $form = array();
  if(variable_get('ponto_keychain_cipher_key', null) != null) {
    $cipher_key = variable_get('ponto_keychain_cipher_key', '');
  }
  else {
    $cipher_key = variable_get('drupal_private_key', '');
  }
  $form['ponto_keychain_cipher_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Cipher Key'),
    '#description' => t('Cipher key for encryption.'),
    '#default_value' => $cipher_key,
  );
  return system_settings_form($form);
}


function ponto_keychain_cp($user_id = null, $type = 'user') {
  global $user;
  if($type == 'user') {
    $page .= '<h2>' . t('A listing of your personal keychains.') . '</h2>';
    $page .= '<p>' . t('These key chains are availiable for use ONLY by you.') . '</p>';
  }
  else {
    $page .= '<h2>' . t('A listing of the global key chains of the system.') . '</h2>';
    $page .= '<p>' . t('These key chains are availiable for use by ANYONE on the system.') . '</p>';
  }
  
  
  $rs = db_query("SELECT nid, title FROM {node} WHERE uid = %d AND type = '%s'", $user->uid, 'ponto_keychain');
  $rows = array();
  $header = array(t('Personal Key Chains'));
  while($r = db_fetch_object($rs)) {
    $row = array();
    $row[] = l($r->title, 'node/' . $r->nid);
    $rows[] = $row;
  }
  
  # Keychain Action Box
  drupal_add_css(drupal_get_path('module', 'ponto_keychain') . '/media/ponto_keychain.css');
  $box = "";
  $box .= '<div class="keychain-action-box">';
  $box .= "Choose an action.";
  $box .= '</div>';
  $box = "";
  $images_path = drupal_get_path('module', 'ponto_keychain') . '/media/images';
  $box .= '<p>' . theme_image("$images_path/key_add.png", t('Add Keychain'), t('Add Key Chain')) . ' - ' . t('Add a Key Chain') . '</p>';
  $page .= $box;
  
  if(count($rows) == 0) {
    $page .= '<strong>' . t('No key chains where found.') . '</strong>';
  }
  else {
    $page .= '<p>' . t('You have %num key chains.', array('%num' => count($rows))) . '</p>';
    $page .= theme('table', $header, $rows);
  }
  return $page;
}

/**
 * Includes all the neccessary js, css, and .class files.
 */
function _ponto_keychain_init() {
  $dir_path = drupal_get_path("module", "ponto_keychain") . "/includes";
  _ponto_keychain_include_files($dir_path, false, 'Interface');
  // All others next
  _ponto_keychain_include_files($dir_path);
  return;
}

/**
 * Helper function that returns an array of keychains.
 * @param int $type Keychain type
 * @return
 */
function _ponto_keychain_get_keychains($type = PONTO_KEYCHAIN_TYPE_USER, $user_id = null) {
  global $user;
  if(user_access('ponto kc admin')) {
    if($user_id == null) {
      $user_id = $user->uid;
    }
  }
  else {
    $user_id = $user->uid;
  }
  // Check that the user can access system types
  if(!user_access('ponto kc admin')) {
    $type = PONTO_KEYCHAIN_TYPE_USER;
  }
  if($type == PONTO_KEYCHAIN_TYPE_USER) {
      $sql = "SELECT kcid, kc_title FROM {ponto_keychain} pc
      INNER JOIN {node} n WHERE pc.kcid = n.nid AND n.uid = %d
      AND pc.kc_type = %d OR pc.kc_type = %d";
      
      $rs = db_query($sql, $user_id, PONTO_KEYCHAIN_TYPE_USER, PONTO_KEYCHAIN_TYPE_GLOBAL);
  }
  else {
      $sql = "SELECT kcid, kc_title FROM {ponto_keychain} WHERE kc_type = %d OR kc_type = %d";
      $rs = db_query($sql, PONTO_KEYCHAIN_TYPE_SYSTEM, PONTO_KEYCHAIN_TYPE_GLOBAL);    
  }
  $chains = array();
  while($row = db_fetch_object($rs)) {
    $chains[$row->kcid] = $row->kc_title;
  }
  if(count($chains) == 0) {
    $chains[] = '[' . t('None Found') . ']';
  }
  return $chains;
}

/**
 * Helper function to create a new system
 * keychain.
 * This is a helpful functions for other modules to call to create
 * a system keychain automatically for them.
 * @param String $module_name Name of the module requesting the keychain. e.g. views, workflow_ng
 * @param String $username Username
 * @param String $password Passowrd
 * @param String $ssh_key SSH Key if any
 * @return mixed The KeyChain object that has been created.
 */
function _ponto_keychain_create_system_keychain($module_name, $username = '', $password = '', $ssh_key = null) {
  $node = new stdClass();
  $node->title = $module_name;
  $node->description = 'System keychain created for ' . $module_name;
  $node->kc_username = $username;
  $node->kc_password = $password;
  $node->kc_ssh_key = $ssh_key;
  node_save($node);
  $keychain = new PontoKeyChain($node->nid);
  return $keychain;
}

/**
 * Class Loader
 */
function _ponto_keychain_include_files($dir, $absolute = false, $filter = null) {
  $osinfo = _ponto_keychain_detect_os();
  $sep = $osinfo['path_sep'];
  
  // Handles .svn and other misc files that we don't want to load
  if(strpos(basename($dir),'.') === 0) {
    return;
  }
  
  if(!$absolute) {
    $cwd = getcwd();
    $dir_path = $cwd . '/' . $dir;
    $dir_path = str_replace('/', $sep, $dir_path);
  }
  else {
    $dir_path = str_replace('/', $sep, $dir);
  }

  $files = @scandir($dir_path);
  if(!$files) {
    return;
  }
  
  foreach($files as $k => $filename) {
    $filter_out = false;
    $current_path = ($absolute) ? "$dir_path/$filename" : "$cwd/$dir/$filename";
    $current_path = str_replace('/', $sep, $current_path);
    
    if($filename != '.' && $filename != '..' && substr($filename, 0, 1) != '.') {
      if($filter !== null) {
        if(!ereg($filter, basename($filename))) {
          $filter_out = true;
        }
      }
      
      if( !is_dir($current_path) && !$filter_out) {
        include_once($current_path);
      }
      else {
        _ponto_keychain_include_files($current_path, true, $filter);
      }
    }
    
  }
  return;
}

function _ponto_keychain_detect_os() {
  static $ostype;
  if($ostype != '') {
    return $ostype;
  }
  
  $winos = eregi("windows", strtolower(php_uname()));
  if($winos) {
    $ostype['type'] = 'win';
    $ostype['path_sep'] = '\\';
  }
  else {
    $ostype['type'] = 'linux';
    $ostype['path_sep'] = '/';
  }
  return $ostype;
}

function _ponto_keychain_sanitize_path($path) {
  if( _ponto_keychain_detect_os == 'win' ) {
    return str_replace('/', '\\', $path);
  }
  else {
    return str_replace('\\','/', $path);
  }
}

?>